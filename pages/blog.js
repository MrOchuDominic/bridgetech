import React from 'react'
import Head from "next/head"
const blog = () => {
  return (
    <div>
       <Head>
        <title>BridgeTech | About Us</title>
        <meta
          name="courses"
          content="you can select from our wide range of courses"
        />
        <link rel="icon" href="/favicon.png" />
      </Head>
    </div>
  )
}

export default blog
