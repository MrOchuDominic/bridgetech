import React from 'react'
import Head from "next/head"

const signin = () => {
  return (
    <>
       <Head>
        <title>BridgeTech | Sign In</title>
        <meta
          name="courses"
          content="you can select from our wide range of courses"
        />
        <link rel="icon" href="/favicon.png" />
      </Head>
    </>
  )
}

export default signin
