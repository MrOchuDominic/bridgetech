import React from 'react'
import Head from "next/head"
const login = () => {
  return (
    <>
      
      <Head>
        <title>BridgeTech | Log In</title>
        <meta
          name="courses"
          content="you can select from our wide range of courses"
        />
        <link rel="icon" href="/favicon.png" />
      </Head>
    </>
  )
}

export default login
