import React from 'react'
import Head from "next/head"

const quotes = () => {
  return (
    <>
      <Head>
        <title>BridgeTech | Quotes</title>
        <meta
          name="courses"
          content="you can select from our wide range of courses"
        />
        <link rel="icon" href="/favicon.png" />
      </Head>
      <h1>Quotes</h1>
    </>
  );
}

export default quotes
