import React from 'react'
import Head from "next/head"

const getquotes = () => {
  return (
    <>
       <Head>
        <title>BridgeTech | Get Quotes</title>
        <meta
          name="courses"
          content="you can select from our wide range of courses"
        />
        <link rel="icon" href="/favicon.png" />
      </Head>
    </>
  )
}

export default getquotes
