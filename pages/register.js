import React from 'react'
import Head from "next/head"
const register = () => {
  return (
    <>
       <Head>
        <title>BridgeTech | Register</title>
        <meta
          name="courses"
          content="you can select from our wide range of courses"
        />
        <link rel="icon" href="/favicon.png" />
      </Head>
    </>
  )
}

export default register
